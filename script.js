let studentArray = [];

function addStudent(name) {
	studentArray.push(name);
	console.log(`${name} was added to the student's list.`);
}

function countStudents() {
	console.log(
		`There are a total of ${studentArray.length} students enrolled.`
	);
}

function printStudents() {
	studentArray.sort((a, b) => {
		if (a.toLowerCase() > b.toLowerCase()) {
			return 1;
		} else if (a.toLowerCase() < b.toLowerCase()) {
			return -1;
		} else {
			return 0;
		}
	});
	studentArray.forEach((student) => console.log(student));
}

function findStudent(nameInput) {
	let resultsArr = studentArray.filter((student) => {
		if (nameInput.length > 1) {
			return student.toLowerCase().includes(nameInput.toLowerCase());
		} else if (nameInput.length == 1) {
			return student[0].toLowerCase() == nameInput[0].toLowerCase();
		}
	});

	if (resultsArr.length == 1) {
		console.log(`${resultsArr[0]} is an enrollee.`);
	} else if (resultsArr.length > 1) {
		console.log(`${resultsArr.join(", ")} are enrollees.`);
	} else {
		console.log(`${nameInput} is not an enrollee.`);
	}
}

function addSection(section) {
	console.log(
		studentArray.map((student) => {
			return (student = `${student} - Section ${section}`);
		})
	);
}

function removeStudent(nameInput) {
	let capitalizedNameInput =
		nameInput.slice(0, 1).toUpperCase() + nameInput.slice(1);
	let nameInputIndex = studentArray.indexOf(capitalizedNameInput);
	if (studentArray.includes(capitalizedNameInput)) {
		console.log(`${nameInput} was removed from the student's list.`);
		studentArray.splice(nameInputIndex, 1);
	}
}

addStudent("John");
addStudent("Jane");
addStudent("Joe");
countStudents();
printStudents();
findStudent("joe");
findStudent("bill");
findStudent("j");
addSection("A");
removeStudent("joe");
printStudents();
